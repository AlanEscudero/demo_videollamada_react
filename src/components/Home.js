import React from 'react';
import {Videocall} from "./Videocall";

export class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ''};
  }

  render() {
    return (
      <div className="Home">
        <Videocall />
      </div>
    );
  }
}
