import React from 'react';
import SimpleWebRTC from 'simplewebrtc';
import axios from 'axios';
import './Home.css';
import image from '../images/image.png';

export class Videocall extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            approved: false,
            roomName: "test333",
        };
        this.handleUserMedia = this.handleUserMedia.bind(this);
        this.handleClickNewRoom = this.handleClickNewRoom.bind(this);
        this.handleClickJoinRoom = this.handleClickJoinRoom.bind(this);
        this.handleServicebusCall = this.handleServicebusCall.bind(this);
    }

    handleUserMedia(event){
        this.setState({approved: true});
    }

    handleClickNewRoom(event){
        alert("HELLO "+this.state.roomName);
        //this.setState({roomName: event.target.value});
    }

    handleClickJoinRoom(event){
        alert("THIS IS ME WHO YOU ARE LOOKING FOR "+this.state.roomName);
        //this.setState({roomName: event.target.value});
    }

    async handleServicebusCall(event){
        alert("Llamado");
        var headers = {
            'Authorization': 'SharedAccessSignature sr=https%3a%2f%2fservicebusmediadores.servicebus.windows.net%2fqueuemediadores%2fmessages&sig=RPYzBw4EGL%2fupuOV89etjcSdUVc%2bp4JFgvSwBRGN4ZA%3d&se=1541805274&skn=queuepolicy',
            'ContentType': 'application/atom+xml;type=entry;charset=utf-8',
            'Content-Type': 'application/json'
        }
        var data = {
            '_idMediacion': 3,
            '_status': 1,
            '_dateMod': 'Algomas'
        }
        const blockchainRequest = await axios.post('https://servicebusmediadores.servicebus.windows.net/queuemediadores/messages', data, {"headers" : headers});
        alert(blockchainRequest);        
    }
  
    render() {
        var classNameImage = this.state.approved ? 'Camera-image invisibleComp' : 'Camera-image invisibleComp';
        var classNameVideo = this.state.approved ? 'Camera-video' : 'Camera-video';
        var webrtc = new SimpleWebRTC({
            // the id/element dom element that will hold "our" video
            localVideoEl: 'Camera-video',
            // the id/element dom element that will hold remote videos
            remoteVideosEl: 'Remote-videos',
            // immediately ask for camera access
            autoRequestMedia: false,
        });
        webrtc.on('readyToCall', function () {
            webrtc.joinRoom("test333");
        });
        return (
            <div className="Videocall">
                <h1>Simple WebRTC Messenger</h1>
                <div className="Chat">
                    <label>Room</label>
                    <input type="text" placeholder="Enter room name" id="roomName" name="roomName" className="Chat-label" />
                    <br />
                    <br />
                    <div className="Chat-room">
                        <div id="Chat-room-new" className="Chat-room-new" onClick={this.handleClickNewRoom} >Create Room</div>
                        <div className="Chat-room-or"></div>
                        <div id="Chat-room-join" className="Chat-room-join" onClick={this.handleClickJoinRoom} >Join Room</div>
                    </div>
                </div>
                <div className="Camera">
                    <h4 className="Camera-text">
                        Local Camera
                    </h4>
                    <img  id="Camera-image" className={classNameImage} src={image} />
                    <video id="Camera-video" className={classNameVideo}></video>
                </div>
                <h3 className="Remote-text">Remote Cameras</h3>
                <div id="Remote-videos" className="ui stackable grid">
                    <img className="Remote-image" src={image} />
                    <img className="Remote-image" src={image} />
                    <img className="Remote-image" src={image} />
                    <img className="Remote-image" src={image} />
                </div>
                <br />
                <br />
                <div id="Servicebus-message" className="Servicebus-message" onClick={this.handleServicebusCall}>Send ServiceBus Message</div>
            </div>
        );
    }
  }